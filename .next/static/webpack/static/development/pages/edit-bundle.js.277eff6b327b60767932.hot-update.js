webpackHotUpdate("static/development/pages/edit-bundle.js",{

/***/ "./pages/edit-bundle.js":
/*!******************************!*\
  !*** ./pages/edit-bundle.js ***!
  \******************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _babel_runtime_corejs2_core_js_object_keys__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @babel/runtime-corejs2/core-js/object/keys */ "./node_modules/@babel/runtime-corejs2/core-js/object/keys.js");
/* harmony import */ var _babel_runtime_corejs2_core_js_object_keys__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_babel_runtime_corejs2_core_js_object_keys__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _babel_runtime_corejs2_helpers_esm_objectSpread__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @babel/runtime-corejs2/helpers/esm/objectSpread */ "./node_modules/@babel/runtime-corejs2/helpers/esm/objectSpread.js");
/* harmony import */ var _babel_runtime_corejs2_helpers_esm_classCallCheck__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @babel/runtime-corejs2/helpers/esm/classCallCheck */ "./node_modules/@babel/runtime-corejs2/helpers/esm/classCallCheck.js");
/* harmony import */ var _babel_runtime_corejs2_helpers_esm_createClass__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @babel/runtime-corejs2/helpers/esm/createClass */ "./node_modules/@babel/runtime-corejs2/helpers/esm/createClass.js");
/* harmony import */ var _babel_runtime_corejs2_helpers_esm_possibleConstructorReturn__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @babel/runtime-corejs2/helpers/esm/possibleConstructorReturn */ "./node_modules/@babel/runtime-corejs2/helpers/esm/possibleConstructorReturn.js");
/* harmony import */ var _babel_runtime_corejs2_helpers_esm_getPrototypeOf__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @babel/runtime-corejs2/helpers/esm/getPrototypeOf */ "./node_modules/@babel/runtime-corejs2/helpers/esm/getPrototypeOf.js");
/* harmony import */ var _babel_runtime_corejs2_helpers_esm_assertThisInitialized__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @babel/runtime-corejs2/helpers/esm/assertThisInitialized */ "./node_modules/@babel/runtime-corejs2/helpers/esm/assertThisInitialized.js");
/* harmony import */ var _babel_runtime_corejs2_helpers_esm_inherits__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @babel/runtime-corejs2/helpers/esm/inherits */ "./node_modules/@babel/runtime-corejs2/helpers/esm/inherits.js");
/* harmony import */ var _babel_runtime_corejs2_helpers_esm_defineProperty__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! @babel/runtime-corejs2/helpers/esm/defineProperty */ "./node_modules/@babel/runtime-corejs2/helpers/esm/defineProperty.js");
/* harmony import */ var _babel_runtime_corejs2_helpers_esm_taggedTemplateLiteral__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! @babel/runtime-corejs2/helpers/esm/taggedTemplateLiteral */ "./node_modules/@babel/runtime-corejs2/helpers/esm/taggedTemplateLiteral.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_10___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_10__);
/* harmony import */ var _shopify_polaris__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! @shopify/polaris */ "./node_modules/@shopify/polaris/index.es.js");
/* harmony import */ var store_js__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! store-js */ "./node_modules/store-js/dist/store.legacy.js");
/* harmony import */ var store_js__WEBPACK_IMPORTED_MODULE_12___default = /*#__PURE__*/__webpack_require__.n(store_js__WEBPACK_IMPORTED_MODULE_12__);
/* harmony import */ var graphql_tag__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! graphql-tag */ "./node_modules/graphql-tag/src/index.js");
/* harmony import */ var graphql_tag__WEBPACK_IMPORTED_MODULE_13___default = /*#__PURE__*/__webpack_require__.n(graphql_tag__WEBPACK_IMPORTED_MODULE_13__);
/* harmony import */ var react_apollo__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(/*! react-apollo */ "./node_modules/react-apollo/react-apollo.esm.js");
/* harmony import */ var _components_ResourceList__WEBPACK_IMPORTED_MODULE_15__ = __webpack_require__(/*! ../components/ResourceList */ "./components/ResourceList.js");












function _templateObject() {
  var data = Object(_babel_runtime_corejs2_helpers_esm_taggedTemplateLiteral__WEBPACK_IMPORTED_MODULE_9__["default"])(["\n mutation productVariantUpdate($input: ProductVariantInput!) {\n   productVariantUpdate(input: $input) {\n     product {\n       title\n     }\n     productVariant {\n       id\n       price\n     }\n   }\n }\n"]);

  _templateObject = function _templateObject() {
    return data;
  };

  return data;
}






var img = 'https://cdn.shopify.com/s/files/1/0757/9955/files/empty-state.svg';
var UPDATE_BUNDLE = graphql_tag__WEBPACK_IMPORTED_MODULE_13___default()(_templateObject());

var EditBundle =
/*#__PURE__*/
function (_React$Component) {
  Object(_babel_runtime_corejs2_helpers_esm_inherits__WEBPACK_IMPORTED_MODULE_7__["default"])(EditBundle, _React$Component);

  function EditBundle(props) {
    var _this;

    Object(_babel_runtime_corejs2_helpers_esm_classCallCheck__WEBPACK_IMPORTED_MODULE_2__["default"])(this, EditBundle);

    _this = Object(_babel_runtime_corejs2_helpers_esm_possibleConstructorReturn__WEBPACK_IMPORTED_MODULE_4__["default"])(this, Object(_babel_runtime_corejs2_helpers_esm_getPrototypeOf__WEBPACK_IMPORTED_MODULE_5__["default"])(EditBundle).call(this, props));

    Object(_babel_runtime_corejs2_helpers_esm_defineProperty__WEBPACK_IMPORTED_MODULE_8__["default"])(Object(_babel_runtime_corejs2_helpers_esm_assertThisInitialized__WEBPACK_IMPORTED_MODULE_6__["default"])(_this), "state", {
      title: '',
      variantId: '',
      showToast: false,
      open: false,
      activeSection: false,
      sections: [],
      selectedProducts: []
    });

    Object(_babel_runtime_corejs2_helpers_esm_defineProperty__WEBPACK_IMPORTED_MODULE_8__["default"])(Object(_babel_runtime_corejs2_helpers_esm_assertThisInitialized__WEBPACK_IMPORTED_MODULE_6__["default"])(_this), "handleChange", function (val, node) {
      _this.setState(Object(_babel_runtime_corejs2_helpers_esm_defineProperty__WEBPACK_IMPORTED_MODULE_8__["default"])({}, node, val));
    });

    Object(_babel_runtime_corejs2_helpers_esm_defineProperty__WEBPACK_IMPORTED_MODULE_8__["default"])(Object(_babel_runtime_corejs2_helpers_esm_assertThisInitialized__WEBPACK_IMPORTED_MODULE_6__["default"])(_this), "handleSelection", function (resources, selected) {
      console.log(resources);
      console.log(selected);
      var _this$state = _this.state,
          activeSection = _this$state.activeSection,
          sections = _this$state.sections;
      var idsFromResources = resources.selection.map(function (product) {
        return product.id;
      });
      sections[activeSection] = Object(_babel_runtime_corejs2_helpers_esm_objectSpread__WEBPACK_IMPORTED_MODULE_1__["default"])({}, sections[activeSection], {
        products: idsFromResources,
        selected: idsFromResources
      });

      _this.setState({
        open: false,
        products: sections,
        activeSection: false
      });
    });

    Object(_babel_runtime_corejs2_helpers_esm_defineProperty__WEBPACK_IMPORTED_MODULE_8__["default"])(Object(_babel_runtime_corejs2_helpers_esm_assertThisInitialized__WEBPACK_IMPORTED_MODULE_6__["default"])(_this), "handleSectionChange", function (val, section, ind) {
      console.log(section);
      var storeSections = _this.state.sections;
      storeSections[ind] = Object(_babel_runtime_corejs2_helpers_esm_objectSpread__WEBPACK_IMPORTED_MODULE_1__["default"])({}, storeSections[activeSection], {
        title: val
      });

      _this.setState({
        sections: storeSections
      });
    });

    Object(_babel_runtime_corejs2_helpers_esm_defineProperty__WEBPACK_IMPORTED_MODULE_8__["default"])(Object(_babel_runtime_corejs2_helpers_esm_assertThisInitialized__WEBPACK_IMPORTED_MODULE_6__["default"])(_this), "addSection", function () {
      var sections = _this.state.sections;
      sections.push(_this.createNewSection());

      _this.setState({
        sections: sections
      });

      console.log(_this.state.sections);
    });

    Object(_babel_runtime_corejs2_helpers_esm_defineProperty__WEBPACK_IMPORTED_MODULE_8__["default"])(Object(_babel_runtime_corejs2_helpers_esm_assertThisInitialized__WEBPACK_IMPORTED_MODULE_6__["default"])(_this), "addProducts", function (ind) {
      console.log(ind);

      _this.setState({
        activeSection: ind,
        open: true,
        selectedProducts: _this.state.sections[ind].products
      });

      console.log(_this.state);
    });

    Object(_babel_runtime_corejs2_helpers_esm_defineProperty__WEBPACK_IMPORTED_MODULE_8__["default"])(Object(_babel_runtime_corejs2_helpers_esm_assertThisInitialized__WEBPACK_IMPORTED_MODULE_6__["default"])(_this), "itemToBeConsumed", function () {
      _this.bundle = store_js__WEBPACK_IMPORTED_MODULE_12___default.a.get('item');
      var sections = _this.bundle.node.metafields ? _this.bundle.node.metafields.sections : _this.createNewSection();

      _this.setState({
        title: _this.bundle.node.title,
        price: _this.bundle.node.variants.edges[0].node.price,
        sections: sections
      }); //this.setState({ title : title, price : price, variantId : variantId })

    });

    Object(_babel_runtime_corejs2_helpers_esm_defineProperty__WEBPACK_IMPORTED_MODULE_8__["default"])(Object(_babel_runtime_corejs2_helpers_esm_assertThisInitialized__WEBPACK_IMPORTED_MODULE_6__["default"])(_this), "createNewSection", function () {
      var rand = Math.floor(100000 + Math.random() * 900000);
      var sections = _this.state.sections;
      var newSection = {};
      newSection[rand] = {
        title: '',
        products: [],
        selected: []
      };
      sections.push(newSection);
      return sections;
    });

    _this.bundle = {};
    return _this;
  }

  Object(_babel_runtime_corejs2_helpers_esm_createClass__WEBPACK_IMPORTED_MODULE_3__["default"])(EditBundle, [{
    key: "componentDidMount",
    value: function componentDidMount() {
      this.itemToBeConsumed();
    }
  }, {
    key: "render",
    value: function render() {
      var _this2 = this;

      var _this$state2 = this.state,
          showToast = _this$state2.showToast,
          showError = _this$state2.showError,
          sections = _this$state2.sections;
      return react__WEBPACK_IMPORTED_MODULE_10___default.a.createElement(react_apollo__WEBPACK_IMPORTED_MODULE_14__["Mutation"], {
        mutation: UPDATE_BUNDLE
      }, function (handleSubmit, _ref) {
        var error = _ref.error,
            data = _ref.data;
        console.log(data);
        var showError = error && react__WEBPACK_IMPORTED_MODULE_10___default.a.createElement(_shopify_polaris__WEBPACK_IMPORTED_MODULE_11__["Banner"], {
          status: "critical"
        }, error.message);
        var showToast = data && data.productVariantUpdate && react__WEBPACK_IMPORTED_MODULE_10___default.a.createElement(_shopify_polaris__WEBPACK_IMPORTED_MODULE_11__["Toast"], {
          content: "Sucessfully updated",
          onDismiss: function onDismiss() {
            return _this2.setState({
              showToast: false
            });
          }
        });
        return react__WEBPACK_IMPORTED_MODULE_10___default.a.createElement(_shopify_polaris__WEBPACK_IMPORTED_MODULE_11__["Page"], {
          primaryAction: {
            content: 'Save',
            onAction: function onAction() {
              var productVariableInput = {
                id: variantId,
                price: discount
              };
              handleSubmit({
                variables: {
                  input: productVariableInput
                }
              });
            }
          },
          secondaryActions: [{
            content: 'Cancel'
          }]
        }, _this2.state.activeSection && react__WEBPACK_IMPORTED_MODULE_10___default.a.createElement(_shopify_polaris__WEBPACK_IMPORTED_MODULE_11__["ResourcePicker"], {
          resourceType: "Product",
          showVariants: false,
          open: _this2.state.open,
          onSelection: function onSelection(resources) {
            return _this2.handleSelection(resources, _this2.state.sections[_this2.state.activeSection].selected);
          },
          onCancel: function onCancel() {
            return _this2.setState({
              open: false,
              selectedProducts: []
            });
          },
          selection: _this2.state.sections[_this2.state.activeSection].selected
        }), react__WEBPACK_IMPORTED_MODULE_10___default.a.createElement(_shopify_polaris__WEBPACK_IMPORTED_MODULE_11__["Layout"], null, showToast, react__WEBPACK_IMPORTED_MODULE_10___default.a.createElement(_shopify_polaris__WEBPACK_IMPORTED_MODULE_11__["Layout"].Section, null, showError), react__WEBPACK_IMPORTED_MODULE_10___default.a.createElement(_shopify_polaris__WEBPACK_IMPORTED_MODULE_11__["Layout"].Section, null, _this2.bundle.node && react__WEBPACK_IMPORTED_MODULE_10___default.a.createElement(_shopify_polaris__WEBPACK_IMPORTED_MODULE_11__["DisplayText"], {
          size: "large"
        }, _this2.bundle.node.title)), react__WEBPACK_IMPORTED_MODULE_10___default.a.createElement(_shopify_polaris__WEBPACK_IMPORTED_MODULE_11__["Layout"].AnnotatedSection, {
          title: "Settings",
          description: "Edit the name and price."
        }, react__WEBPACK_IMPORTED_MODULE_10___default.a.createElement(_shopify_polaris__WEBPACK_IMPORTED_MODULE_11__["Layout"].Section, null, react__WEBPACK_IMPORTED_MODULE_10___default.a.createElement(_shopify_polaris__WEBPACK_IMPORTED_MODULE_11__["Form"], null, react__WEBPACK_IMPORTED_MODULE_10___default.a.createElement(_shopify_polaris__WEBPACK_IMPORTED_MODULE_11__["FormLayout"], null, react__WEBPACK_IMPORTED_MODULE_10___default.a.createElement(_shopify_polaris__WEBPACK_IMPORTED_MODULE_11__["TextField"], {
          label: "Bundle Name",
          value: _this2.state.title,
          onChange: function onChange(val) {
            _this2.setState('title', val);
          }
        }), react__WEBPACK_IMPORTED_MODULE_10___default.a.createElement(_shopify_polaris__WEBPACK_IMPORTED_MODULE_11__["TextField"], {
          prefix: "$",
          value: _this2.state.price,
          label: "Price",
          type: "price",
          onChange: function onChange(val) {
            _this2.setState('price', val);
          }
        }))))), react__WEBPACK_IMPORTED_MODULE_10___default.a.createElement(_shopify_polaris__WEBPACK_IMPORTED_MODULE_11__["Layout"].AnnotatedSection, {
          title: "Sections",
          description: "Add or remove sections."
        }, react__WEBPACK_IMPORTED_MODULE_10___default.a.createElement(_shopify_polaris__WEBPACK_IMPORTED_MODULE_11__["Layout"].Section, null, react__WEBPACK_IMPORTED_MODULE_10___default.a.createElement(_shopify_polaris__WEBPACK_IMPORTED_MODULE_11__["Button"], {
          primary: true,
          onClick: function onClick() {
            return _this2.setState({
              sections: _this2.createNewSection()
            });
          }
        }, "Add Section")), _babel_runtime_corejs2_core_js_object_keys__WEBPACK_IMPORTED_MODULE_0___default()(sections).map(function (ind) {
          var section = sections[ind];
          return react__WEBPACK_IMPORTED_MODULE_10___default.a.createElement(_shopify_polaris__WEBPACK_IMPORTED_MODULE_11__["Layout"].Section, {
            key: ind
          }, react__WEBPACK_IMPORTED_MODULE_10___default.a.createElement(_shopify_polaris__WEBPACK_IMPORTED_MODULE_11__["Form"], null, react__WEBPACK_IMPORTED_MODULE_10___default.a.createElement(_shopify_polaris__WEBPACK_IMPORTED_MODULE_11__["Card"], {
            sectioned: true
          }, react__WEBPACK_IMPORTED_MODULE_10___default.a.createElement(_shopify_polaris__WEBPACK_IMPORTED_MODULE_11__["FormLayout"], null, react__WEBPACK_IMPORTED_MODULE_10___default.a.createElement(_shopify_polaris__WEBPACK_IMPORTED_MODULE_11__["TextField"], {
            label: "Section Name",
            value: section.title,
            onChange: function onChange(val) {
              return _this2.handleSectionChange(val, section, ind);
            }
          }), section.products.length > 0 && react__WEBPACK_IMPORTED_MODULE_10___default.a.createElement(_components_ResourceList__WEBPACK_IMPORTED_MODULE_15__["default"], {
            products: section.products
          }), react__WEBPACK_IMPORTED_MODULE_10___default.a.createElement(_shopify_polaris__WEBPACK_IMPORTED_MODULE_11__["Button"], {
            primary: true,
            onClick: function onClick() {
              _this2.addProducts(ind);
            }
          }, "Add Products")))));
        }))));
      });
    }
  }]);

  return EditBundle;
}(react__WEBPACK_IMPORTED_MODULE_10___default.a.Component);

/* harmony default export */ __webpack_exports__["default"] = (EditBundle);

/***/ })

})
//# sourceMappingURL=edit-bundle.js.277eff6b327b60767932.hot-update.js.map