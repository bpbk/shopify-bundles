import {
  EmptyState,
  Toast,
  Layout,
  Page,
  TextField,
  Button,
  Form,
  FormLayout,
  PageActions,
  Banner
} from '@shopify/polaris';
import store from 'store-js';
import gql from 'graphql-tag';
import { Mutation } from 'react-apollo';
import BundleList from '../components/BundleList';
import { Redirect } from '@shopify/app-bridge/actions';
import * as PropTypes from 'prop-types';

const img = 'https://cdn.shopify.com/s/files/1/0757/9955/files/empty-state.svg'

const CREATE_PRODUCT = gql`
  mutation productCreate($input: ProductInput!) {
   productCreate(input: $input) {
     product {
       title
       handle
       descriptionHtml
       id
       metafields (first:10) {
         edges {
           node {
             key
             value
           }
         }
       }
       images(first: 1) {
         edges {
           node {
             originalSrc
             altText
           }
         }
       }
       variants(first: 1) {
         edges {
           node {
             price
             id
           }
         }
       }
     }
     shop {
       id
     }
     userErrors {
       field
       message
     }
   }
  }
`;

class Index extends React.Component {
  state = {
    addingBundle : false,
    price : null,
    title : null,
  };

  componentDidMount () {
    this.forceUpdate();
  }

  render() {
    // const emptyState = !bundles
    const { price, title } = this.state
    return (
      <Mutation
       mutation={CREATE_PRODUCT}
      >
        {(handleSubmit, {error, data}) => {
          {data && data.productCreate &&
            this.handleReceivedData(data)
          }
          const showError = error && (
            <Banner status="critical">{error.message}</Banner>
          );
          const showToast = data && data.productCreate && (
            <Toast
              content="Sucessfully updated"
              onDismiss={() => this.setState({ showToast: false })}
            />
          );
          return (
            <Page
              primaryAction={{
                content: 'Add Bundle',
                onAction: () => this.addBundle,
              }}
            >
            <Layout>
              {showToast}
              <Layout.Section>
                {showError}
              </Layout.Section>
              <Layout.Section>
                <Button
                  primary={true}
                  onClick={() => this.setState({addingBundle : true})}
                >
                  Add Bundle
                </Button>
              </Layout.Section>
              {(this.state.addingBundle && !data) &&
                <Layout.Section>
                  <Form onSubmit={this.handleSubmit}>
                    <FormLayout>
                      <TextField
                        label="Bundle Name"
                        value={title}
                        onChange={this.handleChange}
                      />
                      <TextField
                        prefix="$"
                        value={price}
                        label="Price"
                        type="price"
                        onChange={this.handlePriceChange}
                      />
                    </FormLayout>
                    <PageActions
                      primaryAction={[
                        {
                          content: 'Create Product',
                          onAction: () => {
                            const productInfo = {
                              title : title,
                              vendor : 'Custom Bundle',
                              productType : 'Custom Bundle',
                              // metafields : [
                              //   {
                              //     namespace: 'bundle',
                              //     key: 'sections',
                              //     value: JSON.stringify([]),
                              //     valueType: 'JSON_STRING'
                              //   }
                              // ],
                              variants : [
                                {
                                  title : title,
                                  price : price
                                }
                              ]
                            };
                            handleSubmit({
                              variables: { input: productInfo }
                            });
                          }
                        }
                      ]}
                      secondaryActions={[
                        {
                          content: 'Remove discount'
                        }
                      ]}
                    />
                  </Form>
                </Layout.Section>
              }
              {
                <Layout.Section>
                  <BundleList/>
                </Layout.Section>
              }

            </Layout>
            </Page>
          )
        }}
      </Mutation>
    )
  }

  static contextTypes = {
    polaris: PropTypes.object
  };

  handleReceivedData = (data) => {
    const item = data.productCreate
    const { price, title } = this.state
    const redirect = Redirect.create(this.context.polaris.appBridge);
    const bundle = {
      id : item.product.id,
      title : item.product.title,
      price : item.product.variants.edges[0].node.price,
      variant : item.product.variants.edges[0].node.id,
      shop : item.shop.id,
      errors : item.userErrors
    }
    store.set('bundle', bundle)
    redirect.dispatch(
      Redirect.Action.APP,
      '/edit-bundle'
    );
  }

  handleChange = (val) => {
    this.setState({ title : val })
  }

  handlePriceChange = (val) => {
    this.setState({ price : val })
  }

  handleSubmit = (data) => {
    console.log(data)
  }
}

export default Index;
