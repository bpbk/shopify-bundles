import {
  Banner,
  Card,
  DisplayText,
  Form,
  FormLayout,
  Layout,
  Page,
  PageActions,
  TextField,
  Toast,
} from '@shopify/polaris';
import store from 'store-js';
import gql from 'graphql-tag';
import { Mutation } from 'react-apollo';

const UPDATE_PRICE = gql`
 mutation productVariantUpdate($input: ProductVariantInput!) {
   productVariantUpdate(input: $input) {
     product {
       title
     }
     productVariant {
       id
       price
     }
   }
 }
`;

class EditBundle extends React.Component {
  state = {
    variantId: '',
    showToast: false,
  };

  componentDidMount() {
    this.setState({ discount: this.itemToBeConsumed() });
  }

  render() {
    const { name, price, discount, variantId } = this.state;
    return (
      <Mutation
       mutation={UPDATE_PRICE}
      >
       {(handleSubmit, {error, data}) => {
         const showError = error && (
           <Banner status="critical">{error.message}</Banner>
         );
         const showToast = data && data.productVariantUpdate && (
           <Toast
             content="Sucessfully updated"
             onDismiss={() => this.setState({ showToast: false })}
           />
         );
          return (
            <Page>
              <Layout>
                {showToast}
                <Layout.Section>
                  {showError}
                </Layout.Section>
                <DisplayText size="large">{name}</DisplayText>
                <Form>
                  <Card sectioned>
                    <FormLayout>
                      {Object.keys(sections).map( ind => {
                        const section = sections[ind]
                        return(
                          <FormLayout.Group key={ind}>
                             <TextField
                               label="Section Name"
                               value={section.title}
                               onChange={(val) => this.handleChange(val, section, ind)}
                             />
                             {section.products &&
                               <ResourceListWithProducts products={section.products} />
                             }
                             <Button
                               primary={true}
                               onClick={() => { this.addProducts(ind) }}
                             >
                               Add Products
                             </Button>
                          </FormLayout.Group>
                        )
                      })}
                    </FormLayout>
                  </Card>
                  <PageActions
                    primaryAction={[
                      {
                        content: 'Save',
                        onAction: () => {
                          const productVariableInput = {
                            id: variantId,
                            price: discount,
                          };
                          handleSubmit({
                            variables: { input: productVariableInput }
                          });
                        }
                      }
                    ]}
                    secondaryActions={[
                      {
                        content: 'Remove discount'
                      }
                    ]}
                  />
                </Form>
              </Layout>
            </Page>
          );
        }}
      </Mutation>
    )
  }

  handleSelection = (resources) => {
    const { activeSection, sections } = this.state
    const idsFromResources = resources.selection.map((product) => product.id);
    sections[activeSection] = {
      ...sections[activeSection], products: idsFromResources, selected: resources
    }
    this.setState({
      open: false,
      products: sections,
      activeSection : false
    })
  }

  handleChange = (val, section, ind) => {
    console.log(section)
    const storeSections = this.state.sections
    storeSections[ind] = {
      ...storeSections[activeSection], title : val
    }
    this.setState({ sections: storeSections })
  }

  addProducts = (ind) => {
    console.log(ind)
    this.setState({
      activeSection : ind,
      open : true,
      selectedProducts : this.state.sections[ind].products
    })
    console.log(this.state)
  }
  // 
  // handleChange = field => {
  //   return value => this.setState({ [field]: value });
  // };

  itemToBeConsumed = () => {
    const item = store.get('item');
    const price = item.variants.edges[0].node.price;
    const variantId = item.variants.edges[0].node.id;
    const discounter = price * 0.1;
    this.setState({ price: price, variantId: variantId });
    return (price - discounter).toFixed(2);
  };
}

export default EditBundle;
