import { EmptyState, Layout, Page, ResourcePicker, TextField, Button } from '@shopify/polaris';
import store from 'store-js';
import ResourceListWithProducts from '../components/ResourceList';

const img = 'https://cdn.shopify.com/s/files/1/0757/9955/files/empty-state.svg'
const rand = Math.floor(100000 + Math.random() * 900000);
const firstObj = {}
firstObj[rand] = {
  'title' : 'hi',
  'products' : [],
  'selected' : []
}

class EditBundle extends React.Component {
  state = {
    open: false,
    activeSection : false,
    sections: firstObj,
    selectedProducts: []
  };

  render() {
    const sections = this.state.sections
    const emptyState = !store.get('ids')
    const selectedProducts = this.state.selectedProducts
    // const selectedProducts = this.state.activeSection
    //   ? this.state.sections[this.state.activeSection].selected
    //   : false
    console.log(this.state.selectedProducts)
    return (
      <Page
        primaryAction={{
          content: 'Select products',
          onAction: () => this.setState({ open: true }),
        }}
      >
        <ResourcePicker
          resourceType="Product"
          showVariants={false}
          open={this.state.open}
          onSelection={this.handleSelection}
          onCancel={() => this.setState({ open: false, selectedProducts : [] })}
          selectedItems={['shopify/Product/1587942719511']}
        />

       {emptyState ? (
         <Layout>
           {Object.keys(sections).map( ind => {
             const section = sections[ind]
             return(
               <Layout.Section key={ind}>
                  <TextField
                    label="Section Name"
                    value={section.title}
                    onChange={(val) => this.handleChange(val, section, ind)}
                  />
                  {section.products &&
                    <ResourceListWithProducts products={section.products} />
                  }
                  <Button
                    primary={true}
                    onClick={() => { this.addProducts(ind) }}
                  >
                    Add Products
                  </Button>

                </Layout.Section>
             )
           })}
         </Layout>
       ) : (
         <ResourceListWithProducts />
       )}
      </Page>
    )
  }

  handleSelection = (resources) => {
    const { activeSection, sections } = this.state
    const idsFromResources = resources.selection.map((product) => product.id);
    sections[activeSection] = {
      ...sections[activeSection], products: idsFromResources, selected: resources
    }
    this.setState({
      open: false,
      products: sections,
      activeSection : false
    })
  }

  handleChange = (val, section, ind) => {
    console.log(section)
    const storeSections = this.state.sections
    storeSections[ind] = {
      ...storeSections[activeSection], title : val
    }
    this.setState({ sections: storeSections })
  }

  addProducts = (ind) => {
    console.log(ind)
    this.setState({
      activeSection : ind,
      open : true,
      selectedProducts : this.state.sections[ind].products
    })
    console.log(this.state)
  }
}

export default EditBundle;
