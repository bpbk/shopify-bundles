import {
  Banner,
  Button,
  Card,
  DisplayText,
  Form,
  FormLayout,
  Layout,
  Page,
  PageActions,
  ResourcePicker,
  TextField,
  Toast,
} from '@shopify/polaris';
import store from 'store-js';
import gql from 'graphql-tag';
import { Mutation } from 'react-apollo';
import ResourceListWithProducts from '../components/ResourceList';

const img = 'https://cdn.shopify.com/s/files/1/0757/9955/files/empty-state.svg'

const UPDATE_BUNDLE = gql`
 mutation productUpdate($input: ProductInput!) {
   productUpdate(input: $input) {
     product {
       id
       title
       variants (first: 1) {
         edges {
           node {
             price
             id
             metafields (first: 1) {
               edges {
                 node {
                   namespace
                   key
                   value
                   valueType
                   id
                 }
               }
             }
           }
         }
       }
     }
   }
 }
`;

class EditBundle extends React.Component {
  state = {
    title : '',
    variantId : '',
    showToast : false,
    open : false,
    activeSection : false,
    sections : [],
    selectedProducts : []
  };
  constructor (props) {
    super(props)
    this.bundle = {}
  }

  componentDidMount() {
    this.bundleToBeConsumed()
  }

  render() {
    const { showToast, showError, sections } = this.state
    return (
      <Mutation
       mutation={UPDATE_BUNDLE}
      >
       {(handleSubmit, {error, data}) => {
         const showError = error && (
           <Banner status="critical">{error.message}</Banner>
         );
         const showToast = data && data.productUpdate && (
           <Toast
             content="Sucessfully updated"
             onDismiss={() => this.setState({ showToast: false })}
           />
         );
         if( data && data.productUpdate ) {
           // Set store with updated bundle in case someone refreshes
           this.bundle = {
             ...this.bundle,
             metafield : data.productUpdate.product.variants.edges[0].node.metafields.edges[0].node.id,
           }
           const bundle = {
             ...this.bundle,
             title : this.state.title,
             price : this.state.price,
             sections : JSON.stringify(sections)
           }

           store.set('bundle', bundle)
         }
          return (
            <Page
              primaryAction={{
                content: 'Save',
                onAction: () => {
                  const metaSections = JSON.stringify(sections)

                  // Set bundle inputs
                  const productInput = {
                    id: this.bundle.id,
                    title: this.state.title,
                    variants: [
                      {
                        id: this.bundle.variant,
                        price: this.state.price,
                        metafields: [
                          {
                            key : 'sections',
                            value : metaSections,
                            valueType: 'JSON_STRING',
                            namespace : 'bundle',
                            description : 'bundle sections'
                          }
                        ]
                      }
                    ]
                  };
                  if( this.bundle.metafield ) {
                    productInput.variants[0].metafields[0].id = this.bundle.metafield
                  }
                  handleSubmit({
                    variables: { input: productInput }
                  });
                },
              }}
              secondaryActions={[
                {
                  content: 'Cancel'
                }
              ]}
            >
            {
              this.state.activeSection &&
              <ResourcePicker
                resourceType="Product"
                showVariants={false}
                open={this.state.open}
                onSelection={(resources) => (this.handleSelection(resources))}
                onCancel={() => this.setState({ open: false, selectedProducts : [] })}
                selection={this.state.selectedProducts}
              />
            }
              <Layout>
                {showToast}
                <Layout.Section>
                  {showError}
                </Layout.Section>
                <Layout.Section>
                  {
                    this.bundle &&
                    <DisplayText size="large">{this.bundle.title}</DisplayText>
                  }
                </Layout.Section>
                <Layout.AnnotatedSection
                  title="Settings"
                  description="Edit the name and price."
                >
                  <Layout.Section>
                    <Form>
                      <FormLayout>
                        <TextField
                          label="Bundle Name"
                          value={this.state.title}
                          onChange={(val) => {this.setState({title : val})}}
                        />
                        <TextField
                          prefix="$"
                          value={this.state.price}
                          label="Price"
                          type="price"
                          onChange={(val) => {this.setState({price : val})}}
                        />
                      </FormLayout>
                    </Form>
                  </Layout.Section>
                </Layout.AnnotatedSection>
                <Layout.AnnotatedSection
                  title="Sections"
                  description="Add or remove sections."
                >
                  <Layout.Section>
                    <Button
                      primary={true}
                      onClick={() => this.setState({ sections : this.createNewSection()})}
                    >
                      Add Section
                    </Button>
                  </Layout.Section>
                  {sections.map( section => {
                    return(
                      <Layout.Section key={section.id}>
                        <Form>
                          <Card sectioned>
                            <FormLayout>
                              <TextField
                                label="Section Name"
                                value={section.title}
                                onChange={(val) => this.handleSectionChange(val, section)}
                              />
                              {section.products.length > 0 &&
                                <ResourceListWithProducts products={section.products} />
                              }
                              <Button
                                primary={true}
                                onClick={() => { this.addProducts(section) }}
                              >
                                Add Products
                              </Button>
                            </FormLayout>
                          </Card>
                        </Form>
                      </Layout.Section>
                    )
                  })}
                </Layout.AnnotatedSection>
              </Layout>
            </Page>
          );
        }}
      </Mutation>
    )
  }

  // handleChange = (val, node) => {
  //   this.setState({ [node] :val })
  // }

  handleSelection = (resources, selected) => {
    const { activeSection } = this.state
    const products = activeSection.products
    //resources.selection.map((product) => products.push(product.id));
    const idsFromResources = resources.selection.map((product) => product.id);
    activeSection.products = products.concat(idsFromResources)
    const sections = this.replaceObjectInSections(activeSection)
    console.log(products)
    this.setState({
      open: false,
      sections: sections,
      activeSection : false
    })
  }

  handleSectionChange = (val, section) => {
    section.title = val
    const sections = this.replaceObjectInSections(section)
    this.setState({ sections: sections })
  }

  addSection = () => {
    const sections = this.state.sections
    sections.push(this.createNewSection())
    this.setState({ sections : sections })
  }

  addProducts = (section) => {
    this.setState({
      activeSection : section,
      selectedProducts : section.products,
      // open : true
    })
    this.setState({
      open : true
    })
  }

  bundleToBeConsumed = () => {
    this.bundle = store.get('bundle')
    console.log(this.bundle)
    const sections = this.bundle.sections
      ? JSON.parse(this.bundle.sections)
      : this.createNewSection()
    this.setState({
      title : this.bundle.title,
      price : this.bundle.price,
      sections : sections
    })
    //this.setState({ title : title, price : price, variantId : variantId })
  };

  createNewSection = () => {
    const rand = Math.floor(100000 + Math.random() * 900000);
    const sections = this.state.sections
    const newSection = {
      id : rand,
      title : '',
      products : []
    }
    sections.push(newSection)
    return sections
  }

  replaceObjectInSections = (newSectionObj) => {
    const { activeSection, sections } = this.state
    // Get section index by id
    const index = sections.indexOf(activeSection);
    // Only after getting it, add the new values to the section

    if (index !== -1) {
        sections[index] = newSectionObj;
    }
    return sections
  }

}

export default EditBundle;
