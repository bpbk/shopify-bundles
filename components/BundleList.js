import gql from 'graphql-tag';
import { Query } from 'react-apollo';
import {
  Card,
  ResourceList,
  Stack,
  TextStyle,
  Thumbnail,
} from '@shopify/polaris';
import store from 'store-js';
import { Redirect } from '@shopify/app-bridge/actions';
import * as PropTypes from 'prop-types';

const GET_PRODUCTS_BY_VENDOR = gql`
  {
    products(first:50, query:"product_type:Custom Bundle"){
      edges{
        node{
          title
          handle
          descriptionHtml
          id
          images(first: 1) {
            edges {
              node {
                originalSrc
                altText
              }
            }
          }
          variants(first: 1) {
            edges {
              node {
                price
                id
                metafields(first: 1) {
                  edges {
                    node {
                      key
                      value
                    }
                  }
                }
              }
            }
          }
        }
      }
    }
  }
`;

class BundleList extends React.Component {

  state = {
    item: '',
  };

  static contextTypes = {
    polaris: PropTypes.object
  };

  redirectToBundle = () => {
    const redirect = Redirect.create(this.context.polaris.appBridge);
    redirect.dispatch(
      Redirect.Action.APP,
      '/edit-bundle'
    );
  };

  render() {
    return (
      <Query query={GET_PRODUCTS_BY_VENDOR}>
        {({ data, loading, error }) => {
          if (loading) return <div>Loading…</div>;
          if (error) return <div>{error.message}</div>;
          console.log(data);
          return (
            <Card sectioned>
              <ResourceList
                showHeader
                resourceName={{ singular: 'Bundle', plural: 'Bundles' }}
                items={data.products.edges}
                renderItem={item => {
                  const media = (
                    <Thumbnail
                      source={
                        item.node.images.edges[0]
                          ? item.node.images.edges[0].node.originalSrc
                          : ''
                      }
                      alt={
                        item.node.images.edges[0]
                          ? item.node.images.edges[0].node.altText
                          : ''
                      }
                    />
                  );
                  return (
                    <ResourceList.Item
                      id={item.node.id}
                      media={media}
                      accessibilityLabel={`View details for ${item.node.title}`}
                      onClick={() => {
                        const bundle = {
                          id : item.node.id,
                          sections : item.node.variants.edges[0].node.metafields.edges[0]
                            ? item.node.variants.edges[0].node.metafields.edges[0].node.value
                            : null,
                          title : item.node.title,
                          price : item.node.variants.edges[0].node.price,
                          variant : item.node.variants.edges[0].node.id,
                          metafield : item.node.variants.edges[0].node.metafields
                            ? item.node.variants.edges[0].node.metafields.edges[0].node.id
                            : null,
                          // shop : item.shop.id,
                          // errors : item.userErrors
                        }
                        store.set('bundle', bundle);
                        this.redirectToBundle();
                      }}
                    >
                      <Stack>
                        <Stack.Item fill>
                          <h3>
                            <TextStyle variation="strong">
                              {item.node.title}
                            </TextStyle>
                          </h3>
                        </Stack.Item>
                        <Stack.Item>
                          <p>${item.node.variants.edges[0].node.price}</p>
                        </Stack.Item>
                      </Stack>
                    </ResourceList.Item>
                  );
                }}
              />
            </Card>
          );
        }}
      </Query>
    );
  }
}

export default BundleList;
